class DigitalZoom {
    constructor() {
        this.el = null;
        this.elBaseImgPosition = { dx: 0, dy: 0 };
        this.elSize = { h: 0, w: 0, isUploaded: false };

        this._canvas = null;
        this._ctx = null;

        this._minimap = null;
        this._ctxMinimap = null;

        this.lastScreenX = null;
        this.lastScreenY = null;

        this.dragged = null;
        this.dragStart = null;

        this.zoomHistory = []; // {x:null, y: null, delta:null}

        this.viewportBorders = { e: 0, f: 0 }; // e = x : f = y

        this.lineCoords = {
            1: { begin: { x: 0, y: 0 }, end: { x: 1, y: 0 } },
            2: { begin: { x: 0, y: 0 }, end: { x: 0, y: 1 } },
            3: { begin: { x: 1, y: 0 }, end: { x: 1, y: 1 } },
            4: { begin: { x: 0, y: 1 }, end: { x: 1, y: 1 } },
        };
    }

    init(img) {
        this._canvas = document.getElementById('canvas');
        this._canvas.width = 800;
        this._canvas.height = 600;
        this.el = img;
        img.onload = ({ path }) => {
            this.elSize = {
                h: path[0].height,
                w: path[0].width,
                isUploaded: true,
            };
            this._canvas.width = path[0].width;
            this._canvas.height = path[0].height;
        };

        window.onload = () => {
            this._ctx = this._canvas.getContext('2d');

            this.trackTransforms(this._ctx);
            this.redraw();

            this.lastScreenX = this._canvas.width / 2;
            this.lastScreenY = this._canvas.height / 2;

            this._canvas.addEventListener(
                'DOMMouseScroll',
                event => {
                    this.screenScrollHandler(event, this);
                },
                false,
            );
            this._canvas.addEventListener(
                'mousewheel',
                event => {
                    this.screenScrollHandler(event, this);
                },
                false,
            );
            this._canvas.addEventListener(
                'mousedown',
                event => {
                    this.screenMouseDownHandler(event, this);
                },
                false,
            );
            this._canvas.addEventListener(
                'mousemove',
                event => {
                    this.screenMouseMoveHandler(event, this);
                },
                false,
            );

            this._canvas.addEventListener(
                'mouseup',
                event => {
                    this.screenMouseUpHandler(event, this);
                },
                false,
            );
        };
    }
    zoom(delta) {
        let matrix = this._ctx.getTransform();
        if (matrix.a.toFixed(6) <= 1 && matrix.d.toFixed(6) <= 1 && delta < 0) {
            if (this._minimap) this.deleteMinimap();
            return;
        }
        if (matrix.a.toFixed(6) >= 4 && matrix.d.toFixed(6) >= 4 && delta > 0) {
            return;
        }

        if (delta > 0) {
            this.zoomHistory.push({
                x: this.lastScreenX,
                y: this.lastScreenY,
                delta: matrix,
            });
        }
        if (delta < 0 && this.zoomHistory.length !== 0) {
            if (this.zoomHistory.length === 1) {
                this.zoomHistory.pop();
                this._ctx.scale(1, 1);
                this._ctx.setTransform(1, 0, 0, 1, 0, 0);
                if (this._minimap) this.deleteMinimap();
                this.redraw();
                return;
            }
            const { x, y } = this.zoomHistory.pop();
            this.lastScreenX = x;
            this.lastScreenY = y;
        } else if (this.zoomHistory.length === 0) {
            return;
        }

        let scaleFactor = 1.1;
        let pt = this._ctx.transformedPoint(this.lastScreenX, this.lastScreenY);
        let factor = Math.pow(scaleFactor, delta);

        this._ctx.translate(pt.x, pt.y);
        this._ctx.scale(factor, factor);
        this._ctx.translate(-pt.x, -pt.y);
        matrix = this._ctx.getTransform();
        if (matrix.a >= 1 && matrix.d >= 1 && !this._minimap) {
            this.initMinimap(this.el);
        }
        if (!this.zoomHistory.length && this._minimap) {
            this.deleteMinimap();
        }

        this.redraw();
        this._minimap && this.drawVeiwZone(factor, pt);
    }
    initMinimap(img) {
        this._minimap = document.createElement('canvas');
        this._minimap.setAttribute('id', 'minimap');
        this._minimap.width = this._canvas.width / 2;
        this._minimap.height = this._canvas.height / 2;
        document.body.appendChild(this._minimap);

        /* this._minimap.addEventListener(
            'mousedown',
            event => {
                this.screenMouseDownHandler(event, this);
            },
            false,
        );
        this._minimap.addEventListener(
            'mousemove',
            event => {
                this.screenMouseMoveHandler(event, this);
            },
            false,
        );
        this._minimap.addEventListener(
            'mouseup',
            event => {
                this.screenMouseUpHandler(event, this);
            },
            false,
        );
        */

        this._ctxMinimap = minimap.getContext('2d');
        this.trackTransforms(this._ctxMinimap);
        this._ctxMinimap.drawImage(img, 0, 0, this._canvas.width / 2, this._canvas.height / 2);
    }
    deleteMinimap() {
        this._minimap.remove();
        this._minimap = null;
    }

    drawVeiwZone(factor) {
        this._ctxMinimap.clearRect(
            0,
            0,

            this._minimap.width,
            this._minimap.height,
        );
        this._ctxMinimap.drawImage(this.el, 0, 0, this._canvas.width / 2, this._canvas.height / 2);
        const matrix = this._ctx.getTransform();

        let e = matrix.e / matrix.a / 2;
        let f = matrix.f / matrix.d / 2;

        this._ctxMinimap.beginPath();

        this._ctxMinimap.rect(
            -(matrix.e / matrix.a) / 2,
            -(matrix.f / matrix.d) / 2,
            this._minimap.width / matrix.a,
            this._minimap.height / matrix.d,
        );
        this._ctxMinimap.lineWidth = 1;
        this._ctxMinimap.stroke();
    }
    minimapMouseDownHandler() {}
    minimapMouseUpHandler() {}
    minimapMouseMove() {}
    minimapMouseEnter() {
        const matrix = this._c;
    }
    minimapMouseLeave() {}
    screenScrollHandler(event, self) {
        let delta = event.wheelDelta ? event.wheelDelta / 40 : event.detail ? -event.detail : 0;

        if (delta > 0) {
            self.lastScreenX = event.offsetX || event.pageX - self._canvas.offsetLeft;
            self.lastScreenY = event.offsetY || event.pageY - self._canvas.offsetTop;
        }
        if (delta) self.zoom(delta);
        return event.preventDefault() && false;
    }
    screenMouseDownHandler(event, self) {
        document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect =
            'none';
        self.lastScreenX = event.offsetX || event.pageX - self._canvas.offsetLeft;
        self.lastScreenY = event.offsetY || event.pageY - self._canvas.offsetTop;
        self.dragStart = self._ctx.transformedPoint(self.lastScreenX, self.lastScreenY);
        self.dragged = false;

        const matrix = self._ctx.getTransform(); // calculate borders
        let scaledHeight = self.elSize.h * matrix.a,
            scaledWidth = self.elSize.w * matrix.d;

        self.viewportBorders.e = scaledWidth - self.elSize.w;
        self.viewportBorders.f = scaledHeight - self.elSize.h;

        // self.positionBefore = matrix;
    }
    screenMouseUpHandler(event, self) {
        const { zoomHistory } = self;

        self.dragStart = null;
        const mousePositionX = event.offsetX || event.pageX - self._canvas.offsetLeft;
        const mousePositionY = event.offsetY || event.pageY - self._canvas.offsetTop;

        let currentCoords = self._ctx.getTransform();

        const newRecord = {};
        const deltaX = (self.lastScreenX - mousePositionX) * zoomHistory[zoomHistory.length - 1].delta.a;
        const deltaY = (self.lastScreenY - mousePositionY) * zoomHistory[zoomHistory.length - 1].delta.d;

        newRecord.x = zoomHistory[zoomHistory.length - 1].x + deltaX;
        newRecord.y = zoomHistory[zoomHistory.length - 1].y + deltaY;
        newRecord.delta = zoomHistory[zoomHistory.length - 1].delta;

        zoomHistory[zoomHistory.length - 1] = newRecord;

        self.zoomHistory = zoomHistory;
        console.log(self.zoomHistory);
    }
    screenMouseMoveHandler(event, self) {
        if (self.dragStart && self.viewportBorders.e && self.viewportBorders.f) {
            self.dragged = true;

            let pt = self._ctx.transformedPoint(
                event.offsetX || event.pageX - self._canvas.offsetLeft,
                event.offsetY || event.pageY - self.canvas.offsetTop,
            );
            const matrix = self._ctx.getTransform();
            if (
                -matrix.e >= self.viewportBorders.e ||
                -matrix.f >= self.viewportBorders.f ||
                matrix.e >= 0 ||
                matrix.f >= 0
            ) {
                self._ctx.restore();
                self.redraw();
                self.dragStart = false;
                return;
            } else {
                self._ctx.save();
            }

            self._ctx.translate(pt.x - self.dragStart.x, pt.y - self.dragStart.y);
            self.redraw();
            self.drawVeiwZone();
        }
    }
    redraw() {
        this._ctx.save();
        this._ctx.setTransform(1, 0, 0, 1, 0, 0);
        this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
        this._ctx.restore();
        this._ctx.drawImage(this.el, this.elBaseImgPosition.dx, this.elBaseImgPosition.dy);
    }

    trackTransforms(ctx) {
        let svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        let xform = svg.createSVGMatrix();
        ctx.getTransform = function() {
            return xform;
        };

        let savedTransforms = [];
        let save = ctx.save;
        ctx.save = function() {
            savedTransforms.push(xform.translate(0, 0));
            return save.call(ctx);
        };
        let restore = ctx.restore;
        ctx.restore = function() {
            xform = savedTransforms.pop();
            return restore.call(ctx);
        };

        let scale = ctx.scale;
        ctx.scale = function(sx, sy) {
            xform = xform.scaleNonUniform(sx, sy);
            return scale.call(ctx, sx, sy);
        };
        let rotate = ctx.rotate;
        ctx.rotate = function(radians) {
            xform = xform.rotate(radians * 180 / Math.PI);
            return rotate.call(ctx, radians);
        };
        let translate = ctx.translate;
        ctx.translate = function(dx, dy) {
            xform = xform.translate(dx, dy);
            return translate.call(ctx, dx, dy);
        };
        let transform = ctx.transform;
        ctx.transform = function(a, b, c, d, e, f) {
            let m2 = svg.createSVGMatrix();
            m2.a = a;
            m2.b = b;
            m2.c = c;
            m2.d = d;
            m2.e = e;
            m2.f = f;
            xform = xform.multiply(m2);
            return transform.call(ctx, a, b, c, d, e, f);
        };
        let setTransform = ctx.setTransform;
        ctx.setTransform = function(a, b, c, d, e, f) {
            xform.a = a;
            xform.b = b;
            xform.c = c;
            xform.d = d;
            xform.e = e;
            xform.f = f;
            return setTransform.call(ctx, a, b, c, d, e, f);
        };
        ctx.transformedPoint = function(x, y) {
            let pt = svg.createSVGPoint();
            pt.x = x;
            pt.y = y;
            return pt.matrixTransform(xform.inverse());
        };
    }
}

const dig = new DigitalZoom();
let gkhead = new Image();
gkhead.src = 'http://phrogz.net/tmp/gkhead.jpg'; // 409x446
dig.init(gkhead);
