class DigitalZoom {
  // getTransform
  constructor() {
    this._node = null;

    this.isMousePressed = false;
    this.baseXY = { x: null, y: null };
    this.zoomedXY = { x: null, y: null };
    this.position = {};
    this.layer = 0;
    this.zoom = {
      0: 1,
      1: 1.5,
      2: 2,
      3: 2.5,
      4: 3,
      5: 3.5,
      6: 4,
    };
    this.el = null;
    this._minimap = null;
  }

  init(screen, node = document.querySelector('.screen')) {

    // this._node = node;
    // this.el = screen;
    this.initCanvas();
    // this.baseXY = { x: screen.width, y: screen.height };

    // const ctx = screen.getContext('2d');
    // console.log(ctx);


    // screen.addEventListener('wheel', event => {
    //   return this.onScreenWheel(event, this);
    // });
    
  }
  initCanvas(){
    console.log('object')
    // const canvas = document.getElementById('test');
    // const ctx = canvas.getContext('2d');
    // console.log(ctx); 
    // let img = document.getElementById("scream");
    // ctx.drawImage(img,10,10);
  }
  initMinimap(img) {
    console.log('init');
    const map = document.createElement('img');
    map.src = img.src;
    map.classList = 'minimap';

    this._minimap = map;
    document.body.appendChild(map);

    map.addEventListener(
      'mousedown',
      event => {
        console.log('mousedown');
        return this.onMapMouseDown(event, this);
      },
      false,
    );
    map.addEventListener(
      'mouseup',
      event => {
        console.log('mouseUp');
        return this.onMapMouseUp(event, this);
      },
      false,
    );
    map.addEventListener(
      'mousemove',
      event => {
        return this.onMapMouseMove(event, this);
      },
      false,
    );

    // map.addEventListener('mousedown',function(){

    // })isMousePressed
  }

  deleteMinimap() {
    if (this._minimap) {
      this._minimap.remove();
      this._minimap = null;
    }
  }

  setZoom(layer) {
    const zoom = this.zoom[layer];
    // this.el.style['background-position'] = '100px' + '100px' ;
    this.el.style.transform = `scale(${zoom},${zoom})`;

    if (!this._minimap && this.layer !== 0) {
      this.initMinimap(this.el);
    }
    if (this.layer === 0) {
      this.deleteMinimap();
    }
  }
  onMapMouseMove(event, self) {
    if (self.isMousePressed) {
      const img = self._node.querySelector('.smth');
      let x = event.pageX - self.el.offsetLeft,
        y = event.pageY - self.el.offsetTop,
        left =(self.baseXY.x / self.el.width * x - self._minimap.offsetWidth / 2) *-1,
        top =(self.baseXY.y / self.el.height * y - self._minimap.offsetHeight / 2) *-1;
      //   console.log('position',img);
      // img.style['background-position'] = 100 + 'px' + 100 + 'px';
      // // img.left = 100 + 'px';
    }
  }
  onMapMouseDown(event, self) {
    self.isMousePressed = true;
    event.preventDefault();
  }
  onMapMouseUp(event, self) {
    self.isMousePressed = false;
    event.preventDefault();
  }

  onScreenWheel(event, self) {
    const { wheelDeltaY } = event;

    if (wheelDeltaY > 0 && self.layer < Object.keys(self.zoom).length - 1) {
      self.layer += 1;
      self.setZoom(self.layer);
    } else if (wheelDeltaY < 0 && self.layer !== 0) {
      self.layer -= 1;
      self.setZoom(self.layer);
    }

    if (!self._minimap && self.layer !== 0) {
      self.initMinimap(self.el);
    }
    if (self.layer === 0) {
      self.deleteMinimap();
    }

  }

  minusHandler(event,self){
      self.layer -= 1;
      self.setZoom(self.layer);
  };
  plusHandler(event,self){
    self.layer += 1;
    self.setZoom(self.layer);
  }
}
// const screen = document.createElement('img');
// screen.src = `./generic.jpg`;
// screen.classList.add('smth');
// // document.querySelector('.screen').appendChild(screen);

// const test = new DigitalZoom();

// test.init(screen);

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var img = document.getElementById("scream");
ctx.drawImage(img, 10, 10); 